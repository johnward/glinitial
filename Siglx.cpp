#define SIGLX_IMPL
#include <glad/glad.h>
#include <stdio.h>
#include "stb_image.h"
//#include "Camera.h"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>

#include "Siglx.h"

//#define SIGLX_DEBUG


SiGLX::SiGLX(int height, int width, const char *title)
{
    _height = height;
    _width = width;
    _title = title;

    _lastX = _width / 2.0f;
    _lastY = _height / 2.0f;
}

SiGLX::~SiGLX()
{
    delete _camera;
    _camera = NULL;

    glfwTerminate();
}


#ifdef SIGLX_GLFW



GLFWwindow* SiGLX::CreateWindow()
{
    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif

    // glfw window creation
    // --------------------
    GLFWwindow* window = glfwCreateWindow(_width, _height, _title, NULL, NULL);
    if (window == NULL)
    {
        #ifdef SIGLX_DEBUG
            std::cout << "Failed to create GLFW window" << std::endl;
        #endif

        glfwTerminate();

        return NULL;
    }

    glfwMakeContextCurrent(window);

    return window;
}


int SiGLX::SiGLXLoadFunctionPointers()
{
    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        #ifdef SIGLX_DEBUG
        std::cout << "Failed to initialize GLAD" << std::endl;
        #endif

        return -1;
    }

    return 1;
}

void SiGLX::SiGLXSetInputsAndCallbacks(GLFWwindow* window)
{
    glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
    glfwSetCursorPosCallback(window, mouseCallback);
    glfwSetScrollCallback(window, scrollCallback);
    // tell GLFW to capture our mouse
    //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void SiGLX::SiGLXEnableDepth()
{
    // configure global opengl state
    // -----------------------------
    glEnable(GL_DEPTH_TEST);
}

// utility function for loading a 2D texture from file
// ---------------------------------------------------
unsigned int SiGLX::SiGLXLoadTexture(char const * path)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        #ifdef SIGLX_DEBUG
        std::cout << "Texture failed to load at path: " << path << std::endl;
        #endif

        stbi_image_free(data);
    }

    return textureID;
}

 void SiGLX::SiGLXCreateCamera(glm::vec3 position)
 {
     _camera = new Camera(position);
 }

 glm::vec3 SiGLX::cameraPosition()
 {
     return _camera->Position;
 }

glm::vec3 SiGLX::cameraFront()
{
    return _camera->Front;
}

float SiGLX::cameraZoom()
{
    return _camera->Zoom;
}

glm::mat4 SiGLX::cameraGetViewMatrix()
{
    return glm::lookAt(_camera->Position, _camera->Position + _camera->Front, _camera->Up);
}


void SiGLX::setFrameTime()
{
    float currentFrame = glfwGetTime();
    _deltaTime = currentFrame - _lastFrame;
    _lastFrame = currentFrame;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void SiGLX::processInput(GLFWwindow *window)
{
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        _camera->ProcessKeyboard(FORWARD, _deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        _camera->ProcessKeyboard(BACKWARD, _deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        _camera->ProcessKeyboard(LEFT, _deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        _camera->ProcessKeyboard(RIGHT, _deltaTime);
}

// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
/*void framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and 
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}

// glfw: whenever the mouse moves, this callback is called
// -------------------------------------------------------
void mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    if (sig->_firstMouse)
    {
        sig->_lastX = xpos;
        sig->_lastY = ypos;
        sig->_firstMouse = false;
    }

    float xoffset = xpos - sig->_lastX;
    float yoffset = sig->_lastY - ypos; // reversed since y-coordinates go from bottom to top

    sig->_lastX = xpos;
    sig->_lastY = ypos;

    sig->_camera->ProcessMouseMovement(xoffset, yoffset);
}

// glfw: whenever the mouse scroll wheel scrolls, this callback is called
// ----------------------------------------------------------------------
void scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    sig->_camera->ProcessMouseScroll(yoffset);
}*/

#elif SIGLX_VULKAN
// todo
#endif

