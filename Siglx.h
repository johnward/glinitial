
#ifndef SIGLX_H
#define SIGLX_H

#ifndef STBI_NO_STDIO
#include <stdio.h>
#include <string>
#endif // STBI_NO_STDIO

#define SIGLX_VERSION_MAJOR 3
#define SIGLX_VERSION_MINOR 3

//#define SIGTX_STATIC

/**
 * Graphics API Type
 */
#define SIGLX_OPENGL
//#define SIGLX_VULKAN
//#define SIGLX_METAL

// Windows Framework
#define SIGLX_GLFW

#include "Camera.h"


#ifdef SIGTX_STATIC
#define STBIDEF static
#else
#define STBIDEF extern
#endif

#ifdef SIGLX_GLFW
#include <GLFW/glfw3.h>  // Window Manager
#endif

// Static callback functions
void framebufferSizeCallback(GLFWwindow* window, int width, int height);

void mouseCallback(GLFWwindow* window, double xpos, double ypos);

void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);


class SiGLX
{
    public:
        SiGLX(int height, int width, const char *title);
        ~SiGLX();

        /**
        * Initialise Selected Graphics Stack
        */
        GLFWwindow* CreateWindow();

        /**
         * 
         */
        int SiGLXLoadFunctionPointers();


        void setCallbacks();
        
        // Make OpenGL 3d
        void SiGLXEnableDepth();


        // Process Keyboard input
        void processInput(GLFWwindow *window);

        /**
         * 
         */
        unsigned int SiGLXLoadTexture(char const * path);

        void SiGLXSetInputsAndCallbacks(GLFWwindow* window);

        void SiGLXCreateCamera(glm::vec3 position);

        glm::vec3 cameraPosition();

        glm::vec3 cameraFront();

        float cameraZoom();

        glm::mat4 cameraGetViewMatrix();


        void setFrameTime();

    private:
        int _width;
        int _height;
        const char *_title;

    public:
        // Camera
        Camera* _camera;
        bool _firstMouse = true;
        float _lastX;
        float _lastY;

        // timing
        float _deltaTime = 0.0f;
        float _lastFrame = 0.0f;

};

#endif